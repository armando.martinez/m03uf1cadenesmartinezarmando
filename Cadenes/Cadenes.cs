﻿/*
 * AUTHOR: Armando Martínez
 * DATE: 30-11-2022
 * DESCRIPTION: Conjunt d'exercicis sobre l'ús de strings
 */

using System;

namespace Cadenes
{
    class cadenes
    {
        // Comproba si dues cadenes son EXACTAMENT IGUALS
        public void SonIguals()
        {
            Console.Write("Escriu un string: ");
            string str1 = Console.ReadLine();
            Console.Write("Escriu un altre string: ");
            int comparison = str1.CompareTo(Console.ReadLine());
            if (comparison == 0) { Console.WriteLine("Són iguals"); }
            else { Console.WriteLine("No són iguals"); }
        }
        // Comproba si dues cadenes son IGUALS, independentment de mayúscules o minuscules
        public void SonIguals2()
        {
            Console.Write("Escriu un string: ");
            string str = Console.ReadLine();
            Console.Write("Escriu un altre string: ");
            int comparison = str.ToUpper().CompareTo(Console.ReadLine().ToUpper());
            if (comparison == 0) { Console.WriteLine("Són iguals"); }
            else { Console.WriteLine("No són iguals"); }
        }
        //Elimina els caracters que demani l'usuari d'un string
        public void PurgaCaracters()
        {
            Console.Write("Escriu un string: ");
            string str = Console.ReadLine();
            Console.WriteLine("Escriu un a un els caràcters que vols extreure, introdueix 0 per terminar.");
            char purge;
            do
            {
                purge = Convert.ToChar(Console.ReadLine());
                str = str.Replace(Convert.ToString(purge), "");
            } while (purge != '0');
            Console.WriteLine(str);
        }
        // Substitueix totes les instancies d'un caracter dins d'un string
        public void SubstitueixCaracter()
        {
            Console.Write("Escriu un string: ");
            string str = Console.ReadLine();
            Console.Write("Escriu un caràcter a reemplaçar: ");
            char c = Convert.ToChar(Console.ReadLine());
            Console.Write("Amb que ho vols reemplaçar: ");
            str = str.Replace(c, Convert.ToChar(Console.ReadLine()));
            Console.WriteLine(str);
        }
        // Comprova si dues cadenes d'ADN son del mateix tamany i si es així et diu la seva distancia de Hamming
        public void DistanciaHamming()
        {
            Console.Write("Escriu la teva primera cadena d'ADN [G||C||A||T]: ");
            string adn1 = Console.ReadLine();
            Console.Write("Escriu la teva segona cadena d'ADN [G||C||A||T]: ");
            string adn2 = Console.ReadLine();
            if (adn1.Length == adn2.Length)
            {
                int count = 0;
                Console.WriteLine("Entrada vàlida");
                for (int i = 0; i < adn1.Length; i++)
                {
                    if (adn1[i] != adn2[i]) { count++; }
                }
                Console.WriteLine($"La diferencia de Hamming es de {count}");
            }
            else { Console.WriteLine("Entrada NO vàlida"); }
        }
        // Comprova que una cadena de parentesis tanca correctament
        public void Parentesis()
        {
            Console.Write("Escriu una cadena de parentesis: ");
            string str = Console.ReadLine();
            int count = 0;
            bool error = false;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '(') { count++; }
                else if (str[i] == ')') { count--; }
                if (count < 0) { error = true; }
            }
            if (error == false && str.EndsWith(')')) { Console.WriteLine("La cadena de paréntesis tanca correctament"); }
            else { Console.WriteLine("La cadena de paréntesis NO tanca correctament"); }
        }
        // Comprova que un string sigui un palindrom
        public void Palindrom()
        {
            Console.Write("Escriu una cadena: ");
            string word = Console.ReadLine();
            string drow = "";
            foreach (char val in word) { drow = val + drow; }
            if (word.CompareTo(drow) == 0) { Console.WriteLine($"{drow} es un palíndrom"); }
            else { Console.WriteLine($"{word} NO es un palíndrom"); }
        }
        // Inverteix els caracters de varies paraules
        public void InverteixParaules()
        {
            Console.Write("Escriu varies paraules, separades per espais: ");
            string str = Console.ReadLine();
            string[] words = str.Split(' ');
            foreach (string word in words)
            {
                string drow = "";
                foreach (char val in word) { drow = val + drow; }
                Console.Write($"{drow} ");
            }
        }
        // Inverteix l'ordre de varies paraules
        public void InverteixParaules2()
        {
            Console.Write("Escriu varies paraules, separades per espais: ");
            string str = Console.ReadLine();
            string[] words = str.Split(' ');
            string[] sdrow = new string[words.Length];
            for (int i = 0; i < words.Length; i++)
            {
                sdrow[i] = words[words.Length - i - 1];
            }
            foreach (string word in sdrow) { Console.Write($"{word} "); }
        }
        // Comproba si una cadena conté la paraula "hola", si es cert diu hola, sino adeu
        public void HolaAdeu()
        {
            string str;
            do
            {
                Console.Write("Escriu una cadena de paraules, sense espais i acaba en punt: ");
                str = Console.ReadLine();
            } while (str[str.Length - 1] != '.');
            bool isHola = false;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == 'h' && i + 3 < str.Length)
                { 
                    if (str[i + 1] == 'o' && str[i + 2] == 'l' && str[i + 3] == 'a') { isHola = true; }
                }
            }
            if (isHola) { Console.WriteLine("hola"); }
            else { Console.WriteLine("adeu"); }
        }

    public bool Menu(bool end)
        {
            string[] exercicis = { "Exit", "SonIguals", "SonIguals2", "PurgaCaracters",
                "SubstitueixCaracter", "DistanciaHamming", "Parentesis", "Palindrom",
                "InverteixParaules", "InverteixParaules2","HolaAdeu"};
            Console.WriteLine("Quin exercici vols executar?\n");
            for (int i = 0; i < exercicis.Length; i++)
            {
                if (i < 10) { Console.WriteLine($" {i}. - {exercicis[i]}"); }
                else { Console.WriteLine($"{i}. - {exercicis[i]}"); }
            }
            Console.Write("\n Escriu un número: ");
            string option = Console.ReadLine();
            Console.Clear();
            switch (option)
            {
                case "0":
                    end = !end;
                    break;
                case "1":
                    SonIguals();
                    break;
                case "2":
                    SonIguals2();
                    break;
                case "3":
                    PurgaCaracters();
                    break;
                case "4":
                    SubstitueixCaracter();
                    break;
                case "5":
                    DistanciaHamming();
                    break;
                case "6":
                    Parentesis();
                    break;
                case "7":
                    Palindrom();
                    break;
                case "8":
                    InverteixParaules();
                    break;
                case "9":
                    InverteixParaules2();
                    break;
                case "10":
                    HolaAdeu();
                    break;
                default:
                    Console.WriteLine("\nOpció incorrecta!\n");
                    break;
            }
            Console.WriteLine("\n\n[PRESS ENTER TO CONTINUE]");
            Console.ReadLine();
            Console.Clear();
            return end;
        }
        static void Main()
        {
            var menu = new cadenes();
            bool end = false;
            while (!end) { end = menu.Menu(end); }
        }
    }
}
